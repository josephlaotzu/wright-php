###################
What is Wright PHP
###################

Wright PHP is an MVC Framework for Web Application Development - a toolkit - for people
who build web sites using PHP. Its goal is to enable you to develop projects
much faster than you could if you were writing code from scratch, by providing
a rich set of libraries for commonly needed tasks, as well as a simple
interface and logical structure to access these libraries. CodeIgniter lets
you creatively focus on your project by minimizing the amount of code needed
for a given task.

*******************
Release Information
*******************

This repo contains in-development code for future releases.

**************************
Features
**************************

Wright PHP uses Eloquent ORM and Wright Console that allows you to create Controllers
in your terminal.


*******************
Server Requirements
*******************

PHP version 5.4 or newer is recommended.
Latest version of Composer a dependency manager for PHP

It should work on 5.2.4 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.

************
Installation
************

Download Wright PHP and your ready to go!

*******
License
*******

No License Yet

*********
Resources
*********

-  `User Guide <https://www.facebook.com/josephlao13>`_

Report security issues to our `Security Panel <mailto:josephvincentlao@yahoo.com>`_, thank you.

***************
Acknowledgement
***************

Thank you for using Wright PHP