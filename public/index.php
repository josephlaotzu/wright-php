<?php
/*
|--------------------------------------------------------------------------
| Rendering Application
|--------------------------------------------------------------------------
|
| This file contains all rendering algorithm. Dont mess with this file 
|
*/
/**
* Loading System Init to initialize application
*/
require_once __DIR__.'../../system/init.php';


$basePath = str_finish(dirname(__FILE__), '/public');
$controllersDirectory = '../application/controllers';
$modelsDirectory = $basePath . '../application/models';

Illuminate\Support\ClassLoader::register();
Illuminate\Support\ClassLoader::addDirectories(array($controllersDirectory, $modelsDirectory));

$app = new Illuminate\Container\Container;
Illuminate\Support\Facades\Facade::setFacadeApplication($app);

$app['app'] = $app;

$app['env'] = 'local'; 
// Set $app['env'] = 'production';
// if application is ready for deployment

with(new Illuminate\Events\EventServiceProvider($app))->register();
with(new Illuminate\Routing\RoutingServiceProvider($app))->register();

require '../application/config/Routes.php';

$request = Illuminate\Http\Request::createFromGlobals();

try
{
    $response = $app['router']->dispatch($request);
    $response->send();
}
catch(\Symfony\Component\HttpKernel\Exception\NotFoundHttpException $notFound)
{

    require_once __DIR__.'../../system/core/Errors/error_404.php';
    $error = new Error404($notFound);
  
}

$app = new App;