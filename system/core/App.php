<?php

/**
* 
*/
class App 
{

	protected $controller = NULL;

	protected $method = 'index';

	protected $parameter = [];

	public function __construct()
	{	
		$url = $this->parseUrl();

		if (file_exists('../application/controllers/'. $url[0] .'Controller.php')) {
			$this->controller = $url[0].'Controller';
			unset($url[0]);
		}

		if (!empty($this->controller)):

		require_once '../application/controllers/'. $this->controller .'.php';

		$this->controller = new $this->controller;


		if (isset($url[1])) {
			if (method_exists($this->controller, $url[1])) {
				$this->method = $url[1];
				unset($url[1]);
			}
		}

		$this->parameter = $url ? array_values($url) : [];
		call_user_func_array([$this->controller, $this->method], $this->parameter);	

		endif;

	}

	protected function parseUrl(){
		if (isset($_GET['url'])) {
			return $url = explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
		}
	}

}