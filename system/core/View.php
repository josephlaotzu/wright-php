<?php
/**
* 
*/
class View 
{
	protected $_wright_ob_level;
	
	protected $_wright_cached_vars = array();
	
	protected $_wright_view_paths = array('../application/views/' => TRUE);

	function __construct()
	{
		# code...
	}

	public function view($view, $vars = array(), $return = FALSE){
		return $this->_wright_load(array('_wright_view' => $view, '_wright_vars' => $this->_wright_object_to_array($vars), '_wright_return' => $return));
	}

	protected function _wright_load($_wright_data){
		// Set the default data variables
		foreach (array('_wright_view', '_wright_vars', '_wright_path', '_wright_return') as $_wright_val)
		{
			$$_wright_val = isset($_wright_data[$_wright_val]) ? $_wright_data[$_wright_val] : FALSE;
		}

		$file_exists = FALSE;

		// Set the path to the requested file
		if (is_string($_wright_path) && $_wright_path !== '')
		{
			$_wright_x = explode('/', $_wright_path);
			$_wright_file = end($_wright_x);
		}
		else
		{
			$_wright_ext = pathinfo($_wright_view, PATHINFO_EXTENSION);
			$_wright_file = ($_wright_ext === '') ? $_wright_view.'.phtml' : $_wright_view;

			foreach ($this->_wright_view_paths as $_wright_view_file => $cascade)
			{
				if (file_exists($_wright_view_file.$_wright_file))
				{
					$_wright_path = $_wright_view_file.$_wright_file;
					$file_exists = TRUE;
					break;
				}

				if ( ! $cascade)
				{
					break;
				}
			}

		}

		if ( ! $file_exists && ! file_exists($_wright_path))
		{
			$errMsg = 'Unable to load the requested file: '.$_wright_file;
			require_once __DIR__.'/Errors/error_500.php';
			$error = new Error500($errMsg);exit();
		}

		/*
		 * Extract and cache variables
		 *
		 * You can either set variables using the dedicated $this->load->vars()
		 * function or via the second parameter of this function. We'll merge
		 * the two types and cache them so that views that are embedded within
		 * other views can have access to these variables.
		 */
		if (is_array($_wright_vars))
		{
			$this->_wright_cached_vars = array_merge($this->_wright_cached_vars, $_wright_vars);
		}
		extract($this->_wright_cached_vars);

		/*
		 * Buffer the output
		 *
		 * We buffer the output for two reasons:
		 * 1. Speed. You get a significant speed boost.
		 * 2. So that the final rendered template can be post-processed by
		 *	the output class. Why do we need post processing? For one thing,
		 *	in order to show the elapsed page load time. Unless we can
		 *	intercept the content right before it's sent to the browser and
		 *	then stop the timer it won't be accurate.
		 */
		ob_start();

		// If the PHP installation does not support short tags we'll
		// do a little string replacement, changing the short tags
		// to standard PHP echo statements.
		if ( ! View::is_php('5.4') && ! ini_get('short_open_tag'))
		{
			echo eval('?>'.preg_replace('/;*\s*\?>/', '; ?>', str_replace('<?=', '<?php echo ', file_get_contents($_wright_path))));
		}
		else
		{
			include($_wright_path); // include() vs include_once() allows for multiple views with the same name
		}

		// Controller::log_message('info', 'File loaded: '.$_wright_path);

		// Return the file data if requested
		if ($_wright_return === TRUE)
		{
			$buffer = ob_get_contents();
			@ob_end_clean();
			return $buffer;
		}

		/*
		 * Flush the buffer... or buff the flusher?
		 *
		 * In order to permit views to be nested within
		 * other views, we need to flush the content back out whenever
		 * we are beyond the first level of output buffering so that
		 * it can be seen and included properly by the first included
		 * template and any subsequent ones. Oy!
		 */
		if (ob_get_level() > $this->_wright_ob_level + 1)
		{
			ob_end_flush();
		}
		else
		{
			$_wright_wright->output->append_output(ob_get_contents());
			@ob_end_clean();
		}

		return $this;
	}

	protected function _wright_object_to_array($object){
		return is_object($object) ? get_object_vars($object) : $object;
	}

	protected function is_php($version){
		static $_is_php;
		$version = (string) $version;

		if ( ! isset($_is_php[$version]))
		{
			$_is_php[$version] = version_compare(PHP_VERSION, $version, '>=');
		}

		return $_is_php[$version];
	}

}