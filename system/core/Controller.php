<?php
/**
* 
*/
class Controller extends View 
{	

	function __construct()
	{
		$this->view = new View();
	}

	protected function model($model){
		if (file_exists('../application/models/'. $model .'.php')) {
		require_once '../application/models/'. $model .'.php';;
		return new $model();
		}
	}

}