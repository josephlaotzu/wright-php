<?php

/**
* 
*/

use Illuminate\Database\Eloquent\Model as Eloquent;

class Model extends Eloquent
{
    public function __construct($attributes = array())  {
        parent::__construct($attributes); // Eloquent
    }
}