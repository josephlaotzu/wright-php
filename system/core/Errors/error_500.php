<?php
/**
* 
*/
class Error500 extends Controller
{	
	protected $error = [];

	function __construct($file_exists)
	{	
		$this->error['heading'] = "File Not Found";
    	$this->error['message'] = $file_exists;
    	$this->view('errors/404',$this->error);
	}
}