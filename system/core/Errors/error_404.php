<?php
/**
* 
*/
class Error404 extends Controller
{	
	protected $error = [];

	function __construct($notFound)
	{	
		$this->error['heading'] = "Page Not Found";
    	$this->error['message'] = $notFound;
    	$this->view('errors/404',$this->error);
	}
}