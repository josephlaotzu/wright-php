<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MigrateRunCommand extends Command
{
    protected $commandName = 'migrate:run';
    protected $commandDescription = "Runs the migration file";

    protected $commandArgumentName = "name";
    protected $commandArgumentDescription = "What's the migration file name?";

    protected $commandOptionName = "up"; // should be specified like "make:view --plain"
    protected $commandOptionDescription = 'If set, it will run the up method on your migartion';    
    
    protected $commandOption2Name = "down"; // should be specified like "make:view --plain"
    protected $commandOption2Description = 'If set, it will run the dpwn method on your migartion';  
    
    protected $file = '';

    protected function configure()
    {
        $this
            ->setName($this->commandName)
            ->setDescription($this->commandDescription)
            ->addArgument(
                $this->commandArgumentName,
                InputArgument::REQUIRED,
                $this->commandArgumentDescription
            )
            ->addOption(
               $this->commandOptionName,
               null,
               InputOption::VALUE_NONE,
               $this->commandOptionDescription
            )
            ->addOption(
               $this->commandOption2Name,
               null,
               InputOption::VALUE_NONE,
               $this->commandOption2Description
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = ucfirst($input->getArgument($this->commandArgumentName));

        if (!strpos($name, 'Migration')) {
            $name = $name.'Migration';
        }

        $this->file = 'application/migrations/'.$name.'.php';

        if (file_exists($this->file)){
        
        require_once 'application/config/Database.php';
        require_once 'application/migrations/'.$name.'.php';
        $run = new $name;
        
        $output->writeln('Running '.$name.' Migration...');
        if ($input->getOption($this->commandOptionName)) {
            // up function
                $run->up();
        $output->writeln('Success: '.$name.' "up" method executed.');

        }elseif ($input->getOption($this->commandOption2Name)) {
            // down function
                $run->down();
        $output->writeln('Success: '.$name.' "down" method executed.');
        }

        }else{
        $output->writeln('Error: '.$name.' not found!');
        }
    }

}