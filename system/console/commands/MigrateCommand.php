<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MigrateCommand extends Command
{
    protected $commandName = 'make:migration';
    protected $commandDescription = "Generates a new migration";

    protected $commandArgumentName = "name";
    protected $commandArgumentDescription = "What's the migration name?";

    protected $commandOptionName = "create"; // should be specified like "make:view --plain"
    protected $commandOptionDescription = 'If set, it will generate create migration';    
    
    protected $commandOption2Name = "update"; // should be specified like "make:view --plain"
    protected $commandOption2Description = 'If set, it will generate update migration';  
    
    protected $file = '';
    protected $newfile = '';

    protected function configure()
    {
        $this
            ->setName($this->commandName)
            ->setDescription($this->commandDescription)
            ->addArgument(
                $this->commandArgumentName,
                InputArgument::REQUIRED,
                $this->commandArgumentDescription
            )
            ->addOption(
               $this->commandOptionName,
               null,
               InputOption::VALUE_NONE,
               $this->commandOptionDescription
            )
            ->addOption(
               $this->commandOption2Name,
               null,
               InputOption::VALUE_NONE,
               $this->commandOption2Description
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = ucfirst($input->getArgument($this->commandArgumentName));

        if (!strpos($name, 'Migration')) {
            $name = $name.'Migration';
        }

        $this->file = 'system/console/stubs/migrations/migration.stub';

        $this->newfile = 'application/migrations/'.$name.'.php';

        if (file_exists($this->newfile)) {
        $output->writeln('Error: A migration '.$name.' already exists!');
        die();
        }

        if ($input->getOption($this->commandOptionName)) {
        $this->file = 'system/console/stubs/migrations/migration.create.stub';
        }elseif ($input->getOption($this->commandOption2Name)) {
        $this->file = 'system/console/stubs/migrations/migration.update.stub';
        }
        
        if (!copy($this->file, $this->newfile)) {
        echo "Error: Failed to generate $this->file...\n";
        }else
        $output->writeln('Success! '.$name.' Generated.');
    }

}