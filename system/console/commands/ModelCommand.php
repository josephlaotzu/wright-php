<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ModelCommand extends Command
{
    protected $commandName = 'make:model';
    protected $commandDescription = "Generates a new model";

    protected $commandArgumentName = "name";
    protected $commandArgumentDescription = "What's the model name?";

    protected $commandOptionName = "plain"; // should be specified like "make:controller --plain"
    protected $commandOptionDescription = 'If set, it will generate only plain model';    
    protected $file = '';
    protected $newfile = '';

    protected function configure()
    {
        $this
            ->setName($this->commandName)
            ->setDescription($this->commandDescription)
            ->addArgument(
                $this->commandArgumentName,
                InputArgument::REQUIRED,
                $this->commandArgumentDescription
            )
            ->addOption(
               $this->commandOptionName,
               null,
               InputOption::VALUE_NONE,
               $this->commandOptionDescription
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = ucfirst($input->getArgument($this->commandArgumentName));

        $this->file = 'system/console/stubs/models/model.stub';

        $this->newfile = 'application/models/'.$name.'.php';

        if (file_exists($this->newfile)) {
        $output->writeln('Error: A model '.$name.' already exists!');
        die();
        }

        if ($input->getOption($this->commandOptionName)) {
        $this->file = 'system/console/stubs/models/model.plain.stub';
        }
        
        if (!copy($this->file, $this->newfile)) {
        echo "Error: Failed to generate $this->file...\n";
        }else
        $output->writeln('Success! '.$name.' Model Generated.');
    }

}