<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ViewCommand extends Command
{
    protected $commandName = 'make:view';
    protected $commandDescription = "Generates a new view";

    protected $commandArgumentName = "name";
    protected $commandArgumentDescription = "What's the view name?";

    protected $commandOptionName = "plain"; // should be specified like "make:model --plain"
    protected $commandOptionDescription = 'If set, it will generate only plain view';    
    protected $file = '';
    protected $newfile = '';

    protected function configure()
    {
        $this
            ->setName($this->commandName)
            ->setDescription($this->commandDescription)
            ->addArgument(
                $this->commandArgumentName,
                InputArgument::REQUIRED,
                $this->commandArgumentDescription
            )
            ->addOption(
               $this->commandOptionName,
               null,
               InputOption::VALUE_NONE,
               $this->commandOptionDescription
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = lcfirst($input->getArgument($this->commandArgumentName));

        $this->file = 'system/console/stubs/views/view.stub';

        $this->newfile = 'application/views/'.$name.'.phtml';

        if (file_exists($this->newfile)) {
        $output->writeln('Error: A view '.$name.' already exists!');
        die();
        }

        if ($input->getOption($this->commandOptionName)) {
        $this->file = 'system/console/stubs/views/view.plain.stub';
        }
        
        if (!copy($this->file, $this->newfile)) {
        echo "Error: Failed to generate $this->file...\n";
        }else
        $output->writeln('Success! '.$name.' View Generated.');
    }

}