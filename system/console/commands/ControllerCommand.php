<?php
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ControllerCommand extends Command
{
    protected $commandName = 'make:controller';
    protected $commandDescription = "Generates a new controller";

    protected $commandArgumentName = "name";
    protected $commandArgumentDescription = "What's the controller name?";

    protected $commandOptionName = "plain"; // should be specified like "make:view --plain"
    protected $commandOptionDescription = 'If set, it will generate only plain controller';    
    protected $file = '';
    protected $newfile = '';

    protected function configure()
    {
        $this
            ->setName($this->commandName)
            ->setDescription($this->commandDescription)
            ->addArgument(
                $this->commandArgumentName,
                InputArgument::REQUIRED,
                $this->commandArgumentDescription
            )
            ->addOption(
               $this->commandOptionName,
               null,
               InputOption::VALUE_NONE,
               $this->commandOptionDescription
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = ucfirst($input->getArgument($this->commandArgumentName));

        if (!strpos($name, 'Controller')) {
            $name = $name.'Controller';
        }

        $this->file = 'system/console/stubs/controllers/controller.stub';

        $this->newfile = 'application/controllers/'.$name.'.php';

        if (file_exists($this->newfile)) {
        $output->writeln('Error: A controller '.$name.' already exists!');
        die();
        }

        if ($input->getOption($this->commandOptionName)) {
        $this->file = 'system/console/stubs/controllers/controller.plain.stub';
        }
        
        if (!copy($this->file, $this->newfile)) {
        echo "Error: Failed to generate $this->file...\n";
        }else
        $output->writeln('Success! '.$name.' Generated.');
    }

}