<?php
/*
|--------------------------------------------------------------------------
| Initialization of Wright PHP
|--------------------------------------------------------------------------
|
| This file contains all required files thats necessary in order
| to run Wright PHP. It is important that you dont mess with this files
|
*/

/**
* Comment out error_reporting(-1); ini_set('display_errors', 'On'); 
* after developing your application
* for production environment, no errors will be shown.
*/
error_reporting(-1);
ini_set('display_errors', 'On');


/**
* Autoloader loads all dependencies on vendor
*/
require_once __DIR__.'../../vendor/autoload.php';

/**
* Helpers support vendor dependencies
*/
require_once __DIR__.'../../vendor/illuminate/support/helpers.php';

/**
* Wright PHP's Database configuration
*/
require_once __DIR__.'../../application/config/Database.php';

/**
* Loading Core App file that's responsible of 
* connecting Controllers and Models 
*/
require_once __DIR__.'/core/App.php';

/**
* Loading Core Model that extends to Eloquent ORM
*/
require_once __DIR__.'/core/Model.php';

/**
* Loading Core View that's responsible of
* reading controller class
*/
require_once __DIR__.'/core/View.php';

/**
* Loading Core Controller that's responsible of
* reading model class
*/
require_once __DIR__.'/core/Controller.php';

