<?php
/**
*  Sample of a model Eloquent ORM
*/
class User extends Model
{
	protected $table = 'users';

	protected $fillable = ['username', 'password'];

	public $timestamps = false;

}