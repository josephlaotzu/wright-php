<?php
use Illuminate\Support\Facades\Route as Route;
/*
|--------------------------------------------------------------------------
| Routing Configuration
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Wright PHP the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
| Wright PHP uses illuminate routing, this is same as laravel's routing
| but instead of using Route::get() this router uses $app['router']->get()
| 
| Other methods of routing are post(), resourse(), any() and more.
| 
*/
// $app['router']->get('hello', function() {
//     return 'Are you looking for me ?';
// });
// 
// $app['router']->get('index', 'WelcomeController@index');
// ^^ Sample Routing for your application ^^

Route::get('/', 'WelcomeController@index');
Route::get('home', 'WelcomeController@index');