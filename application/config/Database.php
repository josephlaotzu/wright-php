<?php
use Illuminate\Database\Capsule\Manager as Capsule;

/*
|--------------------------------------------------------------------------
| Database Configuration
|--------------------------------------------------------------------------
|
| This file will contain the settings needed to access your database.
| For complete instructions please consult the 'Database Connection'
| page of the User Guide (Soon).
| 
| 
| 
|
*/

$capsule = new Capsule();

$capsule->addConnection([
	'driver' 	=> 'mysql',
	'host'		=> 'localhost',
	'username'	=> '',
	'password' 	=> '',
	'database' 	=> '',
	'charset' 	=> 'utf8',
	'collation' => 'utf8_unicode_ci',
	'prefix' 	=> ''
	]);

$capsule->setAsGlobal();

$capsule->bootEloquent();