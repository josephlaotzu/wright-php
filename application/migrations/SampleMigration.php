<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Capsule\Manager as Wright;

class SampleMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Wright::schema()->create('Students', function (Blueprint $table) {
            $table->integer('student_id')->primary()->unique()->unsigned();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('middle_name')->nullable();
            $table->string('email'); //->unique()
            $table->integer('contact_number');
            $table->string('course');
            $table->tinyInteger('year');
            $table->string('department');
            $table->string('school_or_collage');
            $table->enum('active', array('yes', 'no'))->default('yes');
            $table->timestamps();
        });
            Wright::statement('ALTER TABLE users ADD blaaa integer;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Wright::schema()->drop('Students');
    }
}